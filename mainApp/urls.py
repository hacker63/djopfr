from django.conf.urls import url
from mainApp import views

urlpatterns = [
    url(r'^$', views.workspace, name='workspace'),
    url(r'^complaints$', views.journalPage, name='complaints'),
    url(r'^complaints/new$', views.complaintNew, name='complaintNew'),
    url(r'^profile', views.profilePage, name='profile'),
]
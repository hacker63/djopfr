import datetime

from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.forms import formset_factory
from django.http import HttpResponse
from django.shortcuts import render
from mainApp import models, forms

# Create your views here.
@login_required()
def workspace(request):
    context = {
        'user':request.user,
        'title':'Журнал обращений - главная страница'
    }
    return render(request,'index.html',context)

@login_required()
def journalPage(request):
    context = {
        'user':request.user,
        'title':'Список обращений',
        'journalFunctions': True
    }
    journalList = models.Complaint.objects.filter(dateCreated__gte=datetime.date.today()).order_by('dateCreated')
    paginator = Paginator(journalList, 10)
    page = request.GET.get('page')
    try:
        context['journalList'] = paginator.page(page)
    except PageNotAnInteger:
        context['journalList'] = paginator.page(1)
    except EmptyPage:
        context['journalList'] = paginator.page(paginator.num_pages)
    return render(request,'journal-list.html',context)

@login_required()
def complaintNew(request):
    context = {
        'title': 'Новое обращение',
    }
    FormSet = formset_factory(forms.ComplainForm, extra=1)
    context['formset'] = FormSet
    return render(request,'complaint-new.html',context)

@login_required()
def profilePage(request):
    context = {
        'user': request.user,
        'title': 'Профиль сотрудника'
    }
    return render(request,'profile.html',context)

def angularView(request):
    return render(request, 'angular/index.html', context={})
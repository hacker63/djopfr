from django.core.paginator import EmptyPage
from rest_framework import pagination
from rest_framework.response import Response


class CustomPagination(pagination.PageNumberPagination):
    def get_paginated_response(self, data):
        try:
            prevNum = self.page.previous_page_number()
        except EmptyPage:
            prevNum = None
        try:
            nextNum = self.page.next_page_number()
        except:
            nextNum = None
        return Response({
            'next': nextNum,
            'previous': prevNum,
            'current': self.page.number,
            'of': self.page.paginator.num_pages,
            'count': self.page.paginator.count,
            'results': data
        })
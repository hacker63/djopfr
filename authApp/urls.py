from django.conf.urls import url
from authApp import views

urlpatterns = [
    url(r'^login', views.logIn, name='login'),
    url(r'^logout', views.logOut, name='logout'),
]
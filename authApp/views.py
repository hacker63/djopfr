from django.contrib.auth import authenticate, logout
from django.contrib.auth.forms import AuthenticationForm
from django.http import HttpResponse
from django.shortcuts import render, redirect


# Create your views here.
def logIn(request):
    context = {
        'title':'Журнал обращений - Форма входа',
    }
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if(form.is_valid):
            from django.contrib.auth import login
            user = authenticate(username=request.POST['username'],password=request.POST['password'])
            if user is not None:
                login(request,user)
                return redirect('/')
            else:
                context['error'] = 'Введите правильное имя пользователя или пароль'
        else:
            context['error'] = 'Имя пользователя или пароль не заполнены'
    context['form'] = AuthenticationForm(request)
    return render(request,'auth.html',context)

def logOut(request):
    logout(request)
    return redirect('/')
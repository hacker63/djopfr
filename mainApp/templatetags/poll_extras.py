from django import template


register = template.Library()

def active(url, request):
    if url == request.get_full_path():
        return True
    else:
        return False


register.filter('active', active)
import datetime

from django.contrib.auth.models import Group
from rest_framework import status
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.serializers import User
from djOPFR.paginators import CustomPagination

from mainApp.models import Classificator, Complaint, Rayons, TypeCall
from api_v1.serializers import ThemeSerializer,\
    ComplaintSerializer,\
    UserSerializer,\
    RayonSerializer,\
    CallTypesSerializer, \
    ComplaintWriteSerializer

# Create your views here.

class ThemesView(APIView):
    def get(self, request, format=None):
        parent_id = None
        if 'parent_id' in request.GET:
            parent_id = request.GET['parent_id']
        themes = Classificator.objects.filter(parent_id=parent_id)
        serializer = ThemeSerializer(themes, many=True)
        return Response(serializer.data)

class GetThemeView(APIView):
    def get(self, request, cid):
        themes = Classificator.objects.get(id=cid)
        serializer = ThemeSerializer(themes, many=False)
        return Response(serializer.data)

class ComplaintsView(APIView):
    queryset = Complaint.objects.all()
    serializer_class = ComplaintSerializer
    def get(self, request):
        complaints = self.queryset\
            .filter(dateCreated__gte=datetime.date.today())\
            .order_by('-dateCreated')
        print(datetime.datetime.today())
        paginator = CustomPagination()
        page = paginator.paginate_queryset(complaints, request)
        serializer = ComplaintSerializer(page, many=True, context={'request':request})
        return paginator.get_paginated_response(serializer.data)
    def post(self, request):
        data_post = request.data
        data_post['initiatorComplaint'] = request.user.id
        data_post['dateCreated'] = datetime.datetime.today()
        serializer = ComplaintWriteSerializer(data=data_post)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data ,status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class ComplaintDetailView(APIView):
    def get(self, request, cid):
        complaint = Complaint.objects.get(id=cid)
        serializer = ComplaintSerializer(complaint, many=False)
        return Response(serializer.data)

class getProfile(APIView):
    def get(self, request):
        user = User.objects.get(id=request.user.id)
        serializer = UserSerializer(user)
        return Response(serializer.data)

class RayonView(APIView):
    def get(self, request):
        rayons = Rayons.objects.all()
        serializer = RayonSerializer(rayons, many=True)
        return Response(serializer.data)
    def post(self, request):
        serilizer = RayonSerializer(data=request.data)
        if serilizer.is_valid():
            serilizer.save()
            return  Response(serilizer.data, status=status.HTTP_201_CREATED)
        return Response(serilizer.errors, status=status.HTTP_400_BAD_REQUEST)

class getRayonView(APIView):
    def get(self, request, cid):
        rayon = Rayons.objects.get(id=cid)
        serializer = RayonSerializer(rayon, many=False)
        return Response(serializer.data)

class CallTypesView(APIView):
    def get(self,request):
        callTypes = TypeCall.objects.all()
        serializer = CallTypesSerializer(callTypes, many=True)
        return Response(serializer.data)

class CallTypeIdView(APIView):
    def get(self,request,tid):
        callTypes = TypeCall.objects.get(id=tid)
        serializer = CallTypesSerializer(callTypes, many=False)
        return Response(serializer.data)

class getStatOfTopicsView(APIView):
    def get(self,request):
        complaintsList = Complaint.objects.all()
        for empl in User.objects.all():
            if(empl.groups.filter(name='Сотрудник КЦ').count() == 0):
                complaintsList = complaintsList.exclude(initiatorComplaint=empl)
        if('id' in request.GET):
            themeObject = {}
            reqId = int(request.GET['id'])
            theme = Classificator.objects.get(id=reqId)
            themeObject['id'] = theme.id
            themeObject['name'] = theme.name
            themeObject['number'] = 0
            queryDict = {}
            if ('from' in request.GET or 'to' in request.GET):
                if 'from' in request.GET:
                    queryDict['dateCreated__gte'] = datetime.datetime.strptime(request.GET['from'], '%Y-%m-%d')
                if 'to' in request.GET:
                    queryDict['dateCreated__lte'] = datetime.datetime.strptime(request.GET['to'], '%Y-%m-%d')
            childs = Classificator.objects.filter(parent=theme.id)
            if(childs):
                themeObject['childs'] = []
                for child in childs:
                    dictChild = {}
                    dictChild['id'] = child.id
                    dictChild['name'] = child.name
                    dictChild['number'] = complaintsList.filter(themeComplaint=child.id).filter(**queryDict).count()
                    themeObject['number'] += dictChild['number']
                    themeObject['childs'].append(dictChild)
            else:
                themeObject['number'] = complaintsList.filter(themeComplaint=theme.id).count()
                themeObject['childs'] = None

            return  Response(themeObject)
        else:
            resultListTopics = []
            listThemes = Classificator.objects.filter(parent=None)
            for theme in listThemes:
                objTheme = {
                    'id': theme.id,
                    'name': theme.name
                }
                resultListTopics.append(objTheme)
            return Response(resultListTopics)

class getStatOfEmployesView(APIView):
    def get(self,request):
        dictCompOfEmployes = []
        if('from' in request.GET or 'to' in request.GET):
            queryDict = {}
            if 'from' in request.GET:
                queryDict['dateCreated__gte'] = datetime.datetime.strptime(request.GET['from'],'%Y-%m-%d')
            if 'to' in request.GET:
                queryDict['dateCreated__lt'] = datetime.datetime.strptime(request.GET['to'],'%Y-%m-%d')
            for usr in User.objects.all():
                if usr.groups.filter(name='Сотрудник КЦ'):
                    dictCompOfEmployes.append({
                        'name': usr.last_name + ' ' + usr.first_name,
                        'number': Complaint.objects
                            .filter(initiatorComplaint=usr)
                            .filter(**queryDict)
                            .count()
                    })
        else:
            for usr in User.objects.all():
                if usr.groups.filter(name='Сотрудник КЦ'):
                    dictCompOfEmployes.append({
                        'name': usr.last_name + ' ' + usr.first_name,
                        'number': Complaint.objects.filter(initiatorComplaint=usr).count()
                    })
        return Response(dictCompOfEmployes)

class getPointsForChartView(APIView):
    def get(self,request):
        dictPointsData = []
        dictPointsLabels = []
        if('numberWeekDays' in request.GET):
            dictTemplateWeekdays = {0: 'Понедельник',
                                    1: 'Вторник',
                                    2: 'Среда',
                                    3: 'Четверг',
                                    4: 'Пятница',
                                    5: 'Суббота',
                                    6: 'Воскресенье'}
            for day_number in range(0, int(request.GET['numberWeekDays'])):
                if (day_number == 0):
                    deltaCounter = datetime.date.today() - datetime.timedelta(days=int(request.GET['numberWeekDays']))
                    delta = deltaCounter + datetime.timedelta(days=1)
                deltaCounter = deltaCounter + datetime.timedelta(days=1)
                delta = delta + datetime.timedelta(days=1)
                cmpl_count = Complaint.objects.filter(dateCreated__gte=deltaCounter) \
                    .filter(dateCreated__lte=delta) \
                    .count()
                dictPointsData.append(cmpl_count)
                nameLabel = dictTemplateWeekdays.get(deltaCounter.weekday()) + ' (' + deltaCounter.strftime(
                    '%d.%m.%Y') + ')'
                dictPointsLabels.append(nameLabel)
            return Response({'data': dictPointsData, 'labels': dictPointsLabels})
        elif('from' in request.GET or 'to' in request.GET):
            queryDict = {}
            resultResponse = {'data':[],
                              'labels': []}
            if('from' in request.GET):
                queryDict['dateCreated__gte'] = datetime.datetime.strptime(request.GET['from'], '%Y-%m-%d')
            else:
                queryDict['dateCreated__gte'] = datetime.datetime.today()
            if('to' in request.GET):
                queryDict['dateCreated__lt'] = datetime.datetime.strptime(request.GET['to'], '%Y-%m-%d')
            else:
                queryDict['dateCreated__lt'] = datetime.datetime.today()
            startDate = queryDict['dateCreated__gte']
            users = User.objects.filter(groups__name="Сотрудник КЦ")
            for querySet in range(0,(queryDict['dateCreated__lt'] - queryDict['dateCreated__gte']).days +1):
                resultResponse['labels'].append(startDate.strftime('%d.%m.%Y'))
                countNum = 0
                for user in users:
                    countNum += Complaint.objects.filter(initiatorComplaint=user).filter(dateCreated__gte=startDate).filter(dateCreated__lte=startDate+datetime.timedelta(days=1)).count()
                resultResponse['data'].append(countNum)
                startDate = startDate+datetime.timedelta(days=1)
            return Response(resultResponse)
        else:
            return Response('Никуда не попало')

class getCountComplaints(APIView):
    def get(self, request):
        count = Complaint.objects.all().count()
        return Response(count)

class getCountComplaintsToday(APIView):
    def get(self, request):
        count = Complaint.objects.filter(dateCreated__gte=datetime.date.today()).count()
        return Response(count)

class searchView(APIView):
    queryset = Complaint.objects.all()
    serializer_class = ComplaintSerializer
    def post(self, request):
        postData = request.data
        resultPostData = {}
        for post_key, post_value in postData.items():
            if(post_value and post_key != 'page' and post_key != 'dateCreated'):
                if(post_key == 'lastName' or post_key == 'firstName' or post_key == 'middleName'):
                    resultPostData[post_key + '__icontains'] = post_value
                else:
                    resultPostData[post_key] = post_value
        complaints = self.queryset
        if (postData['dateCreated']):
            complaints = complaints\
                .filter(dateCreated__gte=postData['dateCreated']) \
                .filter(dateCreated__lt=(datetime.datetime.strptime(postData['dateCreated'], '%Y-%m-%d') + datetime.timedelta(days=1)))
        complaints = complaints.filter(**resultPostData).order_by('-dateCreated')
        paginator = CustomPagination()
        page = paginator.paginate_queryset(complaints, request)
        serializer = ComplaintSerializer(page, many=True, context={'request':request})
        return paginator.get_paginated_response(serializer.data)
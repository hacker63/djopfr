from rest_framework.serializers import ModelSerializer
from django.contrib.auth.models import User
import mainApp.models

from mainApp.models import Classificator, Complaint

class UserSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = ['username', 'last_name', 'first_name', 'id', 'email', 'date_joined']

class ThemeSerializer(ModelSerializer):
    class Meta:
        model = Classificator
        fields = '__all__'

class RayonSerializer(ModelSerializer):
    class Meta:
        model = mainApp.models.Rayons
        fields = '__all__'

class CallTypesSerializer(ModelSerializer):
    class Meta:
        model = mainApp.models.TypeCall
        fields = '__all__'

class ComplaintSerializer(ModelSerializer):
    initiatorComplaint = UserSerializer(many=False)
    rayons = RayonSerializer(many=False)
    themeComplaint = ThemeSerializer(many=False)
    typeCall = CallTypesSerializer(many=False)
    class Meta:
        model = Complaint
        fields = '__all__'

class ComplaintWriteSerializer(ModelSerializer):
    class Meta:
        model = Complaint
        fields = '__all__'
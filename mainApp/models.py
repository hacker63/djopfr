import datetime

from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


def get_first_name(self):
    if(self.last_name and self.first_name):
        fullName = '%s %s' % (self.last_name, self.first_name)
    else:
        fullName = '%s' % self.username
    return fullName

User.add_to_class("__str__", get_first_name)

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, unique=True)
    middleName = models.CharField(max_length=150)
    def __str__(self):
        return "%s" % self.user

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    try:
        instance.profile.save()
    except ObjectDoesNotExist:
        Profile.objects.create(user=instance)

class Rayons(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=100, null=False)
    def __str__(self):
        return self.name

class TypeCall(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=150, null=False)
    def __str__(self):
        return self.name

class Classificator(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    name = models.CharField(max_length=100, null=False)
    parent = models.ForeignKey('self',null=True, blank=True)
    def __str__(self):
        return self.name

class Complaint(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    firstName = models.CharField(max_length=100, null=True, verbose_name='Имя')
    middleName = models.CharField(max_length=100, null=True, verbose_name='Отчество')
    lastName = models.CharField(max_length=100, null=True, verbose_name='Фамилия')
    themeComplaint = models.ForeignKey(Classificator, null=False, default=None, verbose_name='Тема обращения')
    telNumberInitial = models.CharField(max_length=100, null=True, verbose_name='Телефон обратившегося')
    textComment = models.TextField(max_length=500, null=True, blank=True, verbose_name='Комментарий')
    dateCreated = models.DateTimeField(auto_now_add=True, verbose_name='Дата создания обращения')
    snils = models.CharField(max_length=50, null=True, verbose_name="Снилс обратившегося")
    rayons = models.ForeignKey(Rayons, null=True, verbose_name="Район обратившегося")
    typeCall = models.ForeignKey(TypeCall, null=True, verbose_name="Тип вызова")
    control = models.BooleanField(null=False, default=False)
    unanswered = models.BooleanField(null=False, default=False)
    initiatorComplaint = models.ForeignKey(User, null=True)
    def __str__(self):
        return "%s - %s" % (self.telNumberInitial,self.dateCreated)
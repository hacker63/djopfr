from django.conf.urls import url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

from api_v1.views import ThemesView,\
    ComplaintsView,\
    ComplaintDetailView,\
    getProfile,\
    RayonView,\
    CallTypesView,\
    getRayonView, \
    GetThemeView,\
    CallTypeIdView, \
    getStatOfTopicsView, \
    getStatOfEmployesView, \
    getPointsForChartView, \
    getCountComplaints, \
    getCountComplaintsToday, \
    searchView

themes_list = ThemesView.as_view()
theme_get = GetThemeView.as_view()
complaints_list = ComplaintsView.as_view()
complaint_detail = ComplaintDetailView.as_view()
profile_detail = getProfile.as_view()
rayon_list = RayonView.as_view()
callTypes_list = CallTypesView.as_view()
rayon_get = getRayonView.as_view()
callType_get = CallTypeIdView.as_view()
# Данные для статистики
statOfTOpics = getStatOfTopicsView.as_view()
statOfEmployes = getStatOfEmployesView.as_view()
getPoints = getPointsForChartView.as_view()
countComplaintsAllView = getCountComplaints.as_view()
countComplaintsTodayView = getCountComplaintsToday.as_view()
searchComplaintsView = searchView.as_view()

router = routers.SimpleRouter()
router.register(r'^themes$', themes_list, themes_list)
router.register(r'^complaints/',complaints_list, base_name='complaintsList')
router.register(r'^complaints/(?P<cid>[0-9]+)/$',complaint_detail, base_name='complaintDetail')

urlpatterns = [
    url(r'^token-verify/', verify_jwt_token),
    url(r'^authenticate/', obtain_jwt_token),
    url(r'^token-refresh/', refresh_jwt_token),
    url(r'^themes/$', themes_list),
    url(r'^themes/(?P<cid>[0-9]+)', theme_get),
    url(r'^complaints/search/', searchComplaintsView),
    url(r'^complaints/count/today', countComplaintsTodayView),
    url(r'^complaints/count', countComplaintsAllView),
    url(r'^complaints/(?P<cid>[0-9]+)', complaint_detail),
    url(r'^complaints/', complaints_list),
    url(r'^rayons/$', rayon_list),
    url(r'^rayons/(?P<cid>[0-9]+)', rayon_get),
    url(r'^call-types/$', callTypes_list),
    url(r'^call-types/(?P<tid>[0-9]+)', callType_get),
    url(r'^profile/', profile_detail),
    url(r'^getStatOfTopics',statOfTOpics),
    url(r'^getStatOfEmployes', statOfEmployes),
    url(r'^getPointsForChart', getPoints),
]
# -*- coding: utf-8 -*-
# Generated by Django 1.11.6 on 2017-10-31 16:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mainApp', '0007_auto_20171031_1652'),
    ]

    operations = [
        migrations.AlterField(
            model_name='complain',
            name='dateCreated',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]

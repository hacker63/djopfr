from django.contrib import admin
from mainApp import models

# Register your models here.
admin.site.register(models.Profile)
admin.site.register(models.Complaint)
admin.site.register(models.Classificator)
admin.site.register(models.Rayons)
admin.site.register(models.TypeCall)